<?php if(!defined('KIRBY')) exit ?>

title: Page
pages: true
files: true
fields:
	title:
		label: Title
		type:  text
	menu_entry:
		label: Menueintrag
		type: text  
	text:
		label: Text
		type:  textarea
