<?php if(!defined('KIRBY')) exit ?>

title: Single Feature
pages: true
files: true
fields:
	title:
		label: Title
		type:  text
	text:
		label: Text
		type: textarea
	image:
		label: Image
		type: select
		options: images
			
	