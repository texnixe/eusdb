<?php if(!defined('KIRBY')) exit ?>

title: Advantages Section
pages: false
files: true
fields:
	title:
		label: Title
		type:  text
	menu_entry:
		label: Menüeintrag
		type: text	
	advantages:
		label: Features
		type:  structure
		fields:
			feature:
				label: Feature Name
				type: text
			description:
				label: Description
				type: textarea
			icon:
				label: Icon
				type: select
				options: query
				query:
					page: ../
					fetch: images
					value: '{{filename}}'
					text: '{{filename}}'			