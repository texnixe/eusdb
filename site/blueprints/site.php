<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: false
files: true
fields:
  title:
    label: 
    	de: Titel
    	en: Title
    type:  text
  seoSettings:
    label: Seo Settings
    type: headline
  author:
    label: 
    	de: Autor
    	en: Author
    type:  text
  description:
    label: 
    	de: Website-Beschreibung
    	en: Description
    type:  textarea
  keywords:
    label: 
    	de: Schlüsselwörter
    	en: Keywords
    type:  tags
  copyright:
    label: Copyright
    type:  textarea 
  generalSettings:
    label: General Settings
    type: headline
  noOfNodes:
  	label: Anzahl der News, die initial angezeigt werden sollen
  	type: number
  	min: 2
  	max: 6  
