<?php if(!defined('KIRBY')) exit ?>

title: Single Feature
pages: false
files: true
fields:
	title:
		label:
			de: Titel
			en: Title
		type:  text
	text:
		label: Text
		type: textarea
	date:
		label:
			de: Datum
			en: Date
		type: date
	