<?php
if(kirby()->request()->ajax()) {

if((isset($_POST['param']) && $_POST['param'] == 'loadNodes')) : 
	if(isset($_POST['offset']) && (isset($_POST['limit']))) :
		$offset = intval($_POST['offset']) ;
		$limit = intval($_POST['limit']);
		
		$nodes = page('news')->children()->visible()->sortBy('date')->flip()->offset($offset)->limit($limit);
		$more = page('news')->children()->visible()->sortBy('date')->flip()->offset($offset+$limit)->limit(1)->count(); 


	



		$html = '';

		foreach($nodes as $node) {
			$html .= '<div class="cd-timeline-block row" >';
			$html .= '<div class="cd-timeline-img cd-icon-clipboard">';
            $html .= '<time datetime="' . $node->date('c') . '">';
           
            $html .= '<span class="day">' . $node->date('d') . '</span><span class="month">' . strftime("%b",$node->date()) . '</span><span class="year">' . $node->date('Y') . '</span></time></div>';
            $html .= '<div class="cd-timeline-content" id="' . $node->uid() . '" data-sr="enter bottom reset, vFactor 0.5, move 100px">';
			$html .= '<h3 class="cd-timeline-title">' . $node->title()->html() . '</h3>';
			$html .= getFirstPara($node->text()->kt());
			$html .= '<div class="reveal-content">' . getTheRest($node->text()->kt()) . '</div>';
			$html .= '<button class="btn reveal">Show more</button>';
			$html .= '</div></div>';
		}
		

		$data[0] = $html;


		$data[1] = $more;


		echo json_encode($data);
	endif;
endif ;	
    
    
}
else {
	go('error');
}