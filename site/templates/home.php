<?php 
	snippet('header');
	snippet('menu') 
?>
<div class="outer-wrapper">
	<?php foreach($sections->limit(1) as $section) {
		snippet($section->uid(), array('section' => $section));
	} ?>
		<div class="logo">
			<button class="nav-trigger closed"><span class="sr-only">Mobile Navigation</span><span class="bar"></span><span class="bar"></span></button>
			<a href="#register" class="btn standard">Jetzt registrieren!</a>
			<a href="<?php echo url() ?>"><img src="<?php echo $site->url() ?>/assets/images/logo.svg" width="50" height="50"><span>euSDB</span></a>
		</div>
	<?php foreach($sections->offset(1) as $section) {
		snippet($section->uid(), array('section' => $section));
	} ?>
	
<?php snippet('footer');?>