<div id="<?php echo $section->uid()?>"	>
<?php foreach($features as $feature) : ?>
<section class="<?php echo $section->uid()?> clearfix" id="<?php echo $feature->uid()?>">
	<div class="section-outer">
		<div class="section-inner clearfix">
			<?php if($count % 2 == 0) :?>
				<?php if ($image = $feature->image()) : ?>
					<div class="feature-image" data-sr="enter left, vFactor 0.1, move 100px">
						<img src="<?php echo $image->url() ?>" alt="">
					</div>
				<?php endif ?>
				<div class="feature-text" data-sr="enter right, vFactor 0.1, move 100px">
					<h2><?php echo $feature->title()->html() ?></h2>
					<?php echo $feature->text()->kt() ?>
				</div>
		
			<?php else :?>	
				
				<div class="feature-text" data-sr="enter left, vFactor 0.1, move 100px">
					<h2><?php echo $feature->title()->html() ?></h2>
					<?php echo $feature->text()->kt() ?>
				</div>
				<?php if ($image = $feature->image()) : ?>
					<div class="feature-image" data-sr="enter right, vFactor 0.1, move 100px">
						<img src="<?php echo $image->url() ?>" alt="">
					</div>
				<?php endif ?>
			<?php endif ?>	
		</div>
	</div>		
</section>
<?php $count++; endforeach ?>
</div>


