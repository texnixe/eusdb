<section class="clearfix" id="<?php echo $section->uid()?>">
	<div class="section-outer">
		<h1><?php echo $section->title()->html() ?></h1>
		<div class="section-inner">
			<?php foreach($advantages as $advantage): ?>
				<div class="advantages-feature" data-sr="enter bottom">
					<div class="advantages-feature-inner" >
						
					 	<h2><?php echo html($advantage['feature']) ?></h2>
					 	<div class="advantage-icon"><?php echo file_get_contents($site->image($advantage['icon'])->root()) ?></div>
					 	<?php echo kirbytext($advantage['description'])?>
				 	</div>
			 	</div>
			<?php $count++; endforeach ?> 
		</div>
	</div>
	<div class="skew"></div>		
</section>


