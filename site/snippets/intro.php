	
<section class="clearfix" id="<?php echo $section->uid()?>">
	<?php snippet('lang-switch') ?>
	<div class="section-outer">
		<div class="section-inner vertical-center">
			<div class="section-title">
				 <h1><?php echo $section->title()->html() ?></h1>
				 <h2><?php echo $section->subtitle()->html() ?></h2>
			</div>
		</div> 
	 </div>
</section>


