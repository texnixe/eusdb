<?php $references = $section->references()->yaml() ?>	
<section class="clearfix" id="<?php echo $section->uid()?>">
	<div class="section-outer">
		<h1><?php echo $section->title()->html() ?></h1>
		<div class="section-inner owl-carousel">
			<?php foreach($references as $reference): ?>
				<div class="reference-outer">
					<div class="reference-inner" >
					 	<img src="<?php echo $section->image($reference['image'])->url() ?>">
				 	</div>
			 	</div>
			<?php endforeach ?> 
		</div>
	</div>		
</section>

