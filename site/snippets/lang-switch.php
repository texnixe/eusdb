<nav class="languages" role="navigation'"> 
	<?php foreach($site->languages() as $language): ?>
		
			<span>
				<a href="<?php echo $page->url($language->code()) ?>"><?php echo $language->code(); ?></a> 
			</span> 

	<?php endforeach;?>
</nav>
