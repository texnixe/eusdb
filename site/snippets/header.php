<!DOCTYPE html>
<html lang="<?php echo $site->language()->code() ?>">
<head>
	<script>
		var isMobile = { 
		Android: function() { return navigator.userAgent.match(/Android/i); }, 
		BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i); }, 
		iOS: function() { return navigator.userAgent.match(/iPhone|iPad|iPod/i); }, 
		Opera: function() { return navigator.userAgent.match(/Opera Mini/i); }, 
		Windows: function() { return navigator.userAgent.match(/IEMobile/i); }, 
		any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()); } };
	</script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<!--[if lt IE 9]> 
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
	<link href="//code.cdn.mozilla.net/fonts/fira.css" rel="stylesheet" type="text/css">
	<?php echo css("assets/owl/dist/assets/owl.carousel.min.css") ?>
	<?php echo css("assets/owl/dist/assets/owl.theme.default.min.css") ?>

	<?php echo css('assets/css/main.css') ?>
	<style> [data-sr] { visibility: hidden; } </style>
	<?php echo js('assets/js/modernizr.js') ?>
</head>
<body>