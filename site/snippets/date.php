<time datetime="<?= $page->date('c')?>">
<? if($site->language() == $site->language()->default()) :?>
	<span class="day"><?= $page->date('d')?></span> 
	<span class="month"><?= strftime("%b",$page->date())?></span> 
	<span class="year"><?= $page->date('Y')?></span>
<? elseif($site->language() != $site->language()->default()) : ?>
	<span class="month"><?= strftime("%b",$page->date())?></span> 
	<span class="day"><?= $page->date('d')?>, </span> 
	<span class="year"><?= $page->date('Y')?></span>
<? endif ?>	
</time>