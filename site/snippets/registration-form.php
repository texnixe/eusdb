<form class="contact-form" action="<?php echo $site->url()?>/#contact" method="post">
<ul class="cf">
   
   <li class="form-group">
         <label for="name">Titel, Vorname, Nachname: *</label>
         <input type="text" id="name" name="name" placeholder="Titel, Vorname, Nachname"  value="" required  />
   </li> 
   
   <li class="form-group">           
          <label for="company">Firma/Institution</label>
          <input class="form-control" type="text" id="company" name="company" placeholder="company" value=""  />
   </li> 
   <li class="form-group">           
          <label for="address">Adresse</label>
          <input class="form-control" type="text" id="address" name="address" placeholder="Address" value=""  />
   </li>
   <li class="form-group">           
          <label for="plz">PLZ</label>
          <input class="form-control" type="text" id="plz" name="plz" placeholder="PLZ" value=""  />
   </li>
   <li class="form-group">           
          <label for="location">Ort</label>
          <input class="form-control" type="text" id="location" name="location" placeholder="City" value=""  />
   </li> 
   <li class="form-group">
      <label for="email">E-Mail *</label>
      <input class="form-control" type="email" name="_from" id="email" placeholder ="mail@beispiel.de" value="" required/>
   </li>
   <li class="form-group">
      <label type="text" name="website" id="website" class="uniform__potty" ></label>
      <input type="text" name="website" id="website" class="uniform__potty" />
   </li>   
</ul>      
<div class="message cf">
* Pflichtfelder
</div>

   <!--div class="message">

   </div-->


   <button class="btn" type="submit" name="_submit" value="">Absenden</button>

</form>