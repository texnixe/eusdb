	
<section class="clearfix" id="<?php echo $section->uid()?>">
	<div class="section-outer">
		<div class="section-inner">
			 <h1><?php echo $section->title()->html() ?></h1>
			 <h2><?php echo $section->subtitle()->html() ?></h2>
			 <?php echo $section->text()->kt() ?>
			 <?php snippet('registration-form') ?>
	 	</div>
	 </div>
</section>


