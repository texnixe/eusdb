<section class="container cd-container" id="<?php echo $section->uid()?>" data-limit="<?php echo $limit ?>">
	<div class="section-outer">
		<h1><?php echo $section->title()->html() ?></h1>
		<div class="section-inner">
			<div id="cd-timeline">
			<?php foreach($nodes as $node) : ?>			
				<div class="cd-timeline-block row">
					<div class="cd-timeline-img cd-icon-clipboard"><?php snippet('date', array('page' => $node)) ?></div>
					<div class="cd-timeline-content" id="<?php echo $node->uid() ?>" data-sr="enter bottom reset, vFactor 0.5, move 100px">
						<h3 class="cd-timeline-title"><?php echo $node->title()->html() ?></h3>
						<?php echo getFirstPara($node->text()->kt())?>
						<div class="reveal-content"><?php echo getTheRest($node->text()->kt()) ?></div>
								<button class="btn reveal">Show more</button>
					</div>
				</div>
			<?php endforeach ?>
			</div>
			<div id="loading-image" style="display: none; text-align:center;"><img src="assets/images/ajax-loader.gif" /></div>
			<?php if($nodeCount > $limit) : ?><button class="btn" id="show-more">Load more</button><?php endif ?>
		</div>
	</div>
</section>