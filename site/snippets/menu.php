<nav class="menu" id="main-menu" role="navigation"> 
  <ul class="nav-toggle-container cf" id="main-menu">
    <?php foreach($pages->visible()->not('intro') as $p): ?>
    	<li class="menulink <?php if($p->isActive()) { echo "current" ;} ?>"><a href="<?php echo url() ?>/#<?php echo $p->uid() ?>"><?php e($p->menu_entry(), $p->menu_entry()->html(), $p->title()->html()) ?></a></li>
    <?php endforeach ?>
  </ul>
</nav>