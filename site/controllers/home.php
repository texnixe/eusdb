<?php

	return function($site, $pages, $page) {

    $sections = $pages->visible();
    $features = page('features')->children()->visible();
    $advantages = page('advantages')->advantages()->yaml();
    if(!$limit = $site->noOfNodes()->int()) {
      $limit = 3;
    }
    $nodes = page('news')->children()->visible()->sortBy('date')->flip()->limit($limit);
    $nodeCount =  page('news')->children()->visible()->count();
    $count = 0;
 
  return compact('sections', 'count', 'features', 'advantages', 'limit', 'nodes', 'nodeCount');

};