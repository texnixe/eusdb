<?php 
   function getFirstPara($string){
        $string = substr($string,0, strpos($string, "</p>")+4);
        return $string;
    } 
    function getTheRest($string) {
    	$string = substr($string, strpos($string, "</p>")+4);
    	return $string;
    }
    function getFirstSentence($string) {
  		$string = implode(' ', array_slice(explode('.', $string), 0, 1));
  		return $string . ".";
}
?>