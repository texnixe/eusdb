
$(document).ready(function() {
$('.nav-trigger').click(function() {
	$('nav').addClass('show');
	//$('.nav-toggle-container').slideToggle('slow');
	if ($(this).hasClass('closed')) {
		$(this).removeClass('closed').addClass('close');
	}
	else if ($(this).hasClass('close')) {
		$('nav').removeClass('show');
		$(this).removeClass('close').addClass('closed');
	}
	
});

if ($(window).width() < 768) {
	$('.menulink').click(function() {
		$('nav').removeClass('show');
		$('.nav-trigger').removeClass('close').addClass('closed');
	});
}

$('#main-menu').onePageNav({
  currentClass: 'current',
  changeHash: true,
  scrollSpeed: 500,
  scrollThreshold: 0.5,
  filter: '',
  easing: 'swing',
  begin: function() {
      //I get fired when the animation is starting
  },
  end: function() {
      //I get fired when the animation is ending
  }
});





	var config = {
        //delay:  'onload',
        vFactor: 0.90,
      };
	window.sr = new scrollReveal(config);
	
	// check if device is mobile (fix background only if desktop)
	$(function() {
	  if (isMobile.any()) {
	  	$('html').addClass('mobile');
	  } 
	  
	  else {
	  	$('html').addClass('desktop');
	  }
	  
	});

	$(window).bind('scroll', function() {
	  var navHeight = $(window).height()-67;	 
	       if ($(window).scrollTop() > navHeight) {
	           $('.logo').addClass('fixed');
	       }
	       else {
	           $('.logo').removeClass('fixed');
	       }
	});

	$(".owl-carousel").owlCarousel({
		loop: true,
		nav: true,
		navText: [
      "<i class='icon-chevron-left icon-white'><</i>",
      "<i class='icon-chevron-right icon-white'>></i>"
      ],
		center: true,
		responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
	});


	var limit = parseInt($( "#news" ).data( "limit" ));
	var offset = limit * 1;

	var loader = function() {
		loadNodes();
       	return false;
    };

    $('#show-more').bind('click', loader);

	function loadNodes() {
		$("#loading-image").show();
		var request = $.ajax({
		    url:'ajax',
		    type: 'post',
		    dataType: 'json',
		    data: {
		    	'offset' : offset,
		    	'limit' : limit,
		    	'param' : 'loadNodes'
		    }
		    
	    });

		request.done(function(data) {
			$("#loading-image").hide();
			if(data[1]=== 0) {
            	$('#show-more').hide();
            }

            $('.cd-timeline-block').last().after(data[0]);
            sr.init();


            
	    });
	    offset += limit;
      }
	

	var count = 0;
    $(document).on('click', '.reveal', function() {
        if (count % 2 === 0) {
            $(this).prev().show('slow');
            $(this).html('Close');
            count++;
        } else {
            $(this).prev().hide('slow');
            $(this).html('Show more');
            count--;
        }
    });

    var $win = $(window);

    $('#intro').each(function(){
        var scroll_speed = 20;
        var $this = $(this);
        $(window).scroll(function() {
            var bgScroll = -(($win.scrollTop() - $this.offset().top)/ scroll_speed);
            var bgPosition = 'center '+ bgScroll + 'px';
            $this.css({ backgroundPosition: bgPosition });
        });
    });


});